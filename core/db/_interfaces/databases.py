from abc import ABC, abstractmethod
from typing import Any, List, Dict

class DatabaseInterface(ABC):

    @abstractmethod
    def connect(self):
        """Establish a connection to the database."""
        pass

    @abstractmethod
    def disconnect(self):
        """Close the connection to the database."""
        pass

    @abstractmethod
    def insert_one(self, data: Dict[str, Any]):
        """Insert a record into the database."""
        pass

    @abstractmethod
    def insert_many(self, data: List[Dict[str, Any]]):
        """Insert many records into the database."""
        pass

    @abstractmethod
    def find(self, query: Dict[str, Any]) -> List[Dict[str, Any]]:
        """Find records in the database."""
        pass

    @abstractmethod
    def update(self, query: Dict[str, Any], data: Dict[str, Any]):
        """Update records in the database."""
        pass

    @abstractmethod
    def delete(self, query: Dict[str, Any]):
        """Delete records from the database."""
        pass


    @abstractmethod
    def check_exists_by_id(self, document_id):
        """Check if a document exists in the database."""
        pass

    @abstractmethod
    def check_exists(self, query):
        """Check if a document exists in the database."""
        pass

    @abstractmethod
    def get_by_id(self, document_id):
        """Get a document by id."""
        pass

    @abstractmethod
    def get_multiple_by_ids(self, document_ids):
        """Get multiple documents by ids."""
        pass

    @abstractmethod
    def delete_by_id(self, document_id):
        """Delete a document by id."""
        pass