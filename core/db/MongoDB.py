from typing import Any, List, Dict

from pymongo import MongoClient
from pymongo.database import Database, Collection

from ._interfaces.databases import DatabaseInterface
from core.logging import setup_logging

logger = setup_logging(__name__)


class MongoDBDatabase(DatabaseInterface):
    def __init__(self, host: str, port: int, user: str, password: str, database: str, auth_source: str = 'admin',
                 auth_mechanism: str = 'SCRAM-SHA-256', collection_name: str = None):
        """Initializes the MongoDB database

        Args:
            host (str): The host of the MongoDB server.
            port (int): The port of the MongoDB server.
            user (str): The username of the MongoDB server.
            password (str): The password of the MongoDB server.
            database (str): The database name of the MongoDB server.
            auth_source (str, optional): The auth source of the MongoDB server. Defaults to 'admin'.
            auth_mechanism (str, optional): The auth mechanism of the MongoDB server. Defaults to 'SCRAM-SHA-256'.
            collection_name (str, optional): The collection name of the MongoDB server. Defaults to None.
        """
        logger.info(f"Initializing MongoDB database", extra={"host": host, "port": port, "user": user})
        self.host: str = host
        self.port: int = port
        self.user: str = user
        self.password: str = password
        self.database_name: str = database
        self.authSource: str = auth_source
        self.authMechanism: str = auth_mechanism
        self.collection_name: str = collection_name
        self.client: MongoClient = None
        self.db: Database = None
        self.collection: Collection = None

    def connect(self):
        """Establish a connection to the database.
        """
        logger.debug(f"Connecting to database", extra={"host": self.host, "port": self.port, "user": self.user,
                                                       "database": self.database_name})
        self.client = MongoClient(
            host=self.host,
            port=self.port,
            username=self.user,
            password=self.password,
            authSource=self.authSource,
            authMechanism=self.authMechanism
        )
        logger.debug(f"Connected to database")
        self.db = self.client[self.database_name]

    def get_collection(self, collection_name: str = None):
        if collection_name:
            collection = self.db[collection_name]
        elif self.collection_name:
            collection = self.db[self.collection_name]
        else:
            raise Exception('No collection name provided')
        return collection

    def disconnect(self):
        """Close the connection to the database.
        """
        logger.debug(f"Disconnecting from database", extra={"host": self.host, "port": self.port})
        if self.client:
            self.client.close()
        logger.debug(f"Disconnected from database", extra={"host": self.host, "port": self.port})

    def insert_one(self, data: Dict[str, Any], collection_name: str = None):
        """Insert a record into the database.

        Args:
            data (Dict[str, Any]): The data to insert into the database.
            collection_name (str, optional): The name of the collection to insert the data into. Defaults to None.

        Returns:
            result: The result of the insert.
        """
        logger.debug(f"Insert document", extra={"collection_name": collection_name, "document": data})
        collection = self.get_collection(collection_name)
        result = collection.insert_one(data)
        logger.debug("Inserted document", extra={"collection_name": collection_name, "inserted": result})
        return result

    def find(self, query: Dict[str, Any], collection_name: str = None) -> List[Dict[str, Any]]:
        """Find records in the database.

        Args:
            query (Dict[str, Any]): The query to use to find records.
            collection_name (str, optional): The name of the collection to find the records in. Defaults to None.

        Returns:
            List[Dict[str, Any]]: The records that were found.
        """
        logger.debug(f"Find document", extra={"collection_name": collection_name, "query": query})
        collection = self.get_collection(collection_name)
        cursor = collection.find(query)
        logger.debug("Found documents", extra={"collection_name": collection_name, "query": query, "found": cursor})
        return list(cursor)

    def update(self, _id: str, data: Dict[str, Any], collection_name: str = None):
        """Update records in the database.

        Args:
            _id (str): The id of the record to update.
            data (Dict[str, Any]): The data to update the records with.
            collection_name (str, optional): The name of the collection to update the records in. Defaults to None.

        Returns:
            int: The number of records that were updated.
        """
        logger.debug(f"Update document", extra={"collection_name": collection_name, "id": _id, "data": data})
        collection = self.get_collection(collection_name)
        query = {"_id": _id}
        result = collection.update_many(query, {'$set': data})
        logger.debug("Updated documents", extra={"collection_name": collection_name, "id": _id, "data": data,
                                                 "updated": result})
        return result.modified_count

    def insert_many(self, documents: List[Dict[str, Any]], collection_name: str = None):
        """Insert many records into the database.

        Args:
            documents (List[Dict[str, Any]]): The data to insert into the database.
            collection_name (str, optional): The name of the collection to insert the data into. Defaults to None.

        Returns:
            List[Dict[str, Any]]: The records that were inserted.
        """
        logger.debug(f"Insert documents", extra={"collection_name": collection_name, "documents": documents})
        collection = self.get_collection(collection_name)
        doc_inserts = collection.insert_many(documents)
        logger.debug("Inserted documents",
                     extra={"collection_name": collection_name, "documents": documents, "inserted": doc_inserts})
        return doc_inserts.inserted_ids

    def check_exists_by_id(self, document_id, collection_name: str = None):
        """Check if a document exists in the database.

        Args:
            document_id (str): The id of the document to check.
            collection_name (str, optional): The name of the collection to check the document in. Defaults to None.

        Returns:
            bool: Whether the document exists.
        """
        logger.debug(f"Check if Id exists", extra={"collection_name": collection_name, "document_id": document_id})
        collection = self.get_collection(collection_name)
        exists = collection.count_documents({"_id": document_id}) > 0
        logger.debug("Checked if Id exists",
                     extra={"collection_name": collection_name, "document_id": document_id, "exists": exists})
        return exists

    def check_exists(self, query, collection_name: str = None):
        """Check if a document exists in the database.

        Args:
            query (Dict[str, Any]): The query to use to find the document.
            collection_name (str, optional): The name of the collection to check the document in. Defaults to None.

        Returns:
            bool: Whether the document exists.
        """

        logger.debug(f"Check if document exists", extra={"collection_name": collection_name, "query": query})
        collection = self.get_collection(collection_name)
        exists = collection.count_documents(query) > 0
        logger.debug("Checked if document exists",
                     extra={"collection_name": collection_name, "query": query, "exists": exists})
        return exists

    def get_by_id(self, document_id, collection_name: str = None):
        """Get a document by id.

        Args:
            document_id (str): The id of the document to get.
            collection_name (str, optional): The name of the collection to get the document from. Defaults to None.

        Returns:
            Dict[str, Any]: The document that was found.
        """
        logger.debug(f"Get document by Id", extra={"collection_name": collection_name, "document_id": document_id})
        collection = self.get_collection(collection_name)
        data = collection.find_one({"_id": document_id})
        logger.debug("Got document by Id", extra={"collection_name": collection_name, "document_id": document_id})
        return data

    def get_multiple_by_ids(self, document_ids, collection_name: str = None):
        """Get multiple documents by ids.

        Args:
            document_ids (List[str]): The ids of the documents to get.
            collection_name (str, optional): The name of the collection to get the documents from. Defaults to None.

        Returns:
            List[Dict[str, Any]]: The documents that were found.
        """
        logger.debug(f"Get documents by Ids", extra={"collection_name": collection_name, "document_ids": document_ids})
        collection = self.get_collection(collection_name)
        docs = list(collection.find({"_id": {"$in": document_ids}}))
        logger.debug("Got documents by Ids",
                     extra={"collection_name": collection_name, "document_ids": document_ids, "docs": len(docs)})
        return docs

    def delete(self, query: Dict[str, Any], collection_name: str = None):
        """Delete records from the database.

        Args:
            query (Dict[str, Any]): The query to use to find records.
            collection_name (str, optional): The name of the collection to delete the records from. Defaults to None.

        Returns:
            int: The number of records that were deleted.
        """
        logger.debug(f"Delete documents", extra={"collection_name": collection_name, "query": query})
        collection = self.get_collection(collection_name)
        result = collection.delete_many(query)
        logger.debug("Deleted documents", extra={"collection_name": collection_name, "query": query, "deleted": result})
        return result.deleted_count

    def delete_by_id(self, document_id, collection_name: str = None):
        """Delete a document by id.
        Args:
            document_id (str): The id of the document to delete.
            collection_name (str, optional): The name of the collection to delete the document from. Defaults to None.
        """
        deleted_count = self.delete({"_id": document_id}, collection_name)
        logger.debug("Deleted document by id",
                    extra={"collection_name": collection_name, "document_id": document_id, "count": deleted_count})
        return deleted_count
