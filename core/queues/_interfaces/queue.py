from abc import ABC, abstractmethod
from typing import Callable, Any, Optional


class BaseQueueInterface(ABC):
    @abstractmethod
    def connect(self):
        """Connect to the queue."""
        pass

    @abstractmethod
    def disconnect(self):
        """Disconnect from the queue."""
        pass

    @abstractmethod
    def send(self, message, topic: Optional[str] = None):
        """Send a message to a topic."""
        pass

    @abstractmethod
    def ack(self, method_frame):
        """Acknowledge a message."""
        pass

    @abstractmethod
    def get_message(self, timeout: Optional[int] = None) -> Any:
        """Poll a topic for a message with an optional timeout."""
        pass

    @abstractmethod
    def subscribe(self, callback: Callable[[Any], None]):
        """Subscribe to a topic with a callback function."""
        pass

