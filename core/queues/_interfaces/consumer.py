from abc import ABC, abstractmethod


class BaseConsumerInterface(ABC):

    @abstractmethod
    def process_message(self, message):
        """Process a message."""
        pass

    @abstractmethod
    def consume(self):
        """Consume messages."""
        pass

    @abstractmethod
    def stop_consuming(self):
        """Stop consuming messages."""
        pass