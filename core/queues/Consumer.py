from concurrent.futures import ThreadPoolExecutor

from core.logging import setup_logging
from . import RabbitMQQueue

logger = setup_logging(__name__)


class BaseConsumer:
    def __init__(self, user: str, password: str, subscribe_topic: str, push_topic: str = None, host: str = 'localhost',
                 port: int = 5672, prefetch_count: int = 1, workers: int = 1):
        self.subscription_topic: str = subscribe_topic
        self.push_topic: str = push_topic
        self.rabbit_mq_user: str = user
        self.rabbit_mq_password: str = password
        self.rabbit_mq_host: str = host
        self.rabbit_mq_port: int = port
        self.prefetch_count: int = prefetch_count
        self.workers: int = workers

        self.queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                   password=self.rabbit_mq_password,
                                   topic=self.subscription_topic,
                                   host=self.rabbit_mq_host,
                                   port=self.rabbit_mq_port,
                                   prefetch_count=self.prefetch_count)

    def parse_message(self, body) -> bool:
        print(f'Got message: {body}')
        return False

    def run(self):
        logger.info(f'Starting consumer with {self.workers} workers')

        def process_message(channel, method, properties, body):
            logger.debug(f'Processing message: {body}')
            message = body.decode('utf-8')
            channel.basic_ack(delivery_tag=method.delivery_tag)
            parsed = self.parse_message(message)
            if not parsed:
                channel.basic_publish(exchange='',
                                      routing_key=self.subscription_topic,
                                      body=message)
            logger.info(f'Finished processing message: {body}', extra={'parsed': parsed})

        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            for i in range(self.workers):
                logger.info(f'Starting worker {i}')
                queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                      password=self.rabbit_mq_password,
                                      topic=self.subscription_topic,
                                      host=self.rabbit_mq_host,
                                      port=self.rabbit_mq_port,
                                      prefetch_count=self.prefetch_count)
                queue.connect()
                executor.submit(queue.subscribe, callback=process_message)