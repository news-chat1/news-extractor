"""The custom log module for the service
"""

import enum
import logging
import os

from pythonjsonlogger import jsonlogger

log_level = os.environ.get('LOG_LEVEL', 'INFO')


class Level(enum.Enum):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL


class StackdriverFormatter(jsonlogger.JsonFormatter, object):
    """Log formatter for Stackdriver logging
    """

    def __init__(
            self, *args, fmt='', style='%', **kwargs):  # pylint: disable=unused-argument
        jsonlogger.JsonFormatter.__init__(self, fmt=fmt, *args, **kwargs)

    def process_log_record(self, log_record):
        log_record['severity'] = log_record['levelname']
        del log_record['levelname']
        return super(StackdriverFormatter, self).process_log_record(
            log_record)


def setup_logging(name):
    """Sets up the logging for the module.

    The Python logging is setup with the StackdriverFormatter formatter. This
    outputs the logs as JSON.

    Typical usage example:
        LOGGER = setup_logging(__name__)
        LOGGER.info('Hello world')

        LOGGER.info('Adding numbers', extra={'a': 1, 'b': 2})

    Args:
        name: The string name of the module

    Returns:
        An instance of the Python logger.
    """

    handler = logging.StreamHandler()
    handler.setLevel(Level[log_level].value)

    formatter = StackdriverFormatter(
        fmt='%(levelname) %(asctime) %(message)')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(Level[log_level].value)
    if not logger.hasHandlers():
        logger.addHandler(handler)

    return logger
