from src.module import ExtractorConsumer
from src.config import rabbitmq_user, rabbitmq_pass, rabbitmq_host, rabbitmq_port, subscription_topic, push_topic, \
    prefetch_count, workers
import nltk

if __name__ == '__main__':
    nltk.download('punkt')
    # nltk.download('all')
    consumer = ExtractorConsumer(
        user=rabbitmq_user, password=rabbitmq_pass, subscribe_topic=subscription_topic, push_topic=push_topic,
        host=rabbitmq_host, port=rabbitmq_port, prefetch_count=prefetch_count, workers=workers
    )

    consumer.run()
