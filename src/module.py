import json
import traceback

from newspaper import Article
from datetime import datetime

from core.logging import setup_logging
from core.queues.Consumer import BaseConsumer

logger = setup_logging(__name__)


class ExtractorConsumer(BaseConsumer):
    def parse_message(self, body) -> bool:
        logger.debug(f'Processing message: {body}')
        try:
            article = Article(url=body)
            article.download()
            article.parse()
            published = article.publish_date
            if published is not None:
                published = published.strftime("%d/%m/%Y %H:%M:%S")
            else:
                published = ''
            if article.text.strip() == '':
                logger.warn(f'Article has no text', extra={'link': body})
                return True
            else:
                data = {
                    'title': article.title,
                    'text': article.text,
                    'url': body,
                    'top_image': article.top_image,
                    'movies': list(article.movies),
                    'images': list(article.images),
                    'authors': article.authors,
                    'publish_date': published,
                    'parse_date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                }

                article.nlp()
                data['summary'] = article.summary
                data['keywords'] = article.keywords
                queue = self.queue
                queue.send(json.dumps(data), topic=self.push_topic)
                logger.info(f'Finished processing message: {body}, {article.text[:100]}...')
        except Exception as e:
            traceback.print_exc()
            logger.error(f'Unable to extract article', extra={'Error': e, 'link': body})
            return False
        return True
