import os

subscription_topic = os.getenv('SUBSCRIPTION_TOPIC')
push_topic = os.getenv('PUSH_QUEUE')
rabbitmq_host = os.getenv('RABBITMQ_HOST', 'localhost')
rabbitmq_port = int(os.getenv('RABBITMQ_PORT', '5672'))
rabbitmq_user = os.getenv('RABBITMQ_USER')
rabbitmq_pass = os.getenv('RABBITMQ_PASS')
prefetch_count = int(os.getenv('PREFETCH_COUNT', '1'))
workers = int(os.getenv('WORKERS', '12'))